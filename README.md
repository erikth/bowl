# Bowling score calculator kata

### Install:

```
$ composer install
```

### Run:

```
$ ./bowl "XXXXXXXXXXXX"
$ ./bowl "9-9-9-9-9-9-9-9-9-9-"
$ ./bowl "5/5/5/5/5/5/5/5/5/5/5"
```

### Test:

```
$ phpunit --bootstrap vendor/autoload.php src/BowlTest
```
