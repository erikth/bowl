<?php
/**
 * Class for scoring 10 pin bowling
 *
 * @author  Erik TH <eth@erikth.net>
 */

class Bowl
{
    /**
     * Score input
     * @var string
     */
    private $score = '';

    /**
     * Multidimentional array of numeric scores of each frame
     * @var array
     */
    private $frames = [];

    /**
     * Total score of the game
     * @var integer
     */
    private $totalScore = 0;

    /**
     * @param string $score
     */
    public function __construct($score = '')
    {
        if (empty($score)) {
            throw new Exception('no score given');
        }

        $this->score = $score;
    }

    /**
     * Parse the given score and tally all pins and bonuses
     * @return integer Total score
     */
    public function run()
    {
        $this->parseScore()
             ->tallyScore();

        return $this->totalScore;
    }

    /**
     * Parse the string input into numerical arrays for processing
     * @return object Bowl
     */
    private function parseScore()
    {
        $frames = str_split($this->score);

        $try1 = null;

        foreach ($frames as $frameNumber => $frame) {
            // if frame is a strike, there is no 2nd try
            if ($frame == 'X') {
                $frames[$frameNumber] = [10];
                continue;
            }

            // first try
            if (is_null($try1)) {
                $try1 = $frame;
                // if gutter ball or foul, no score
                if ($frame == '-' || $frame == 'F') {
                    $try1 = 0;
                }
                $frames[$frameNumber] = [$try1];
            // second try
            } else {
                $score = $frame;
                // picked up a spare
                if ($frame == '/') {
                    $score = 10 - $try1;
                // gutter ball or foul
                } else if ($frame == '-' || $frame == 'F') {
                    $score = 0;
                }

                // add score of both tries to the frames array
                $frames[$frameNumber - 1] = [$try1, $score];

                // reset for the next iteration
                $try1 = null;
                unset($frames[$frameNumber]);
            }
        }

        // reset array indexes
        $frames = array_values($frames);
        // start frames at 1 rather than 0
        array_unshift($frames, null);
        unset($frames[0]);

        $this->frames = $frames;

        return $this;
    }

    /**
     * Tally up the total score of all frames with bonuses
     * @return object Bowl
     */
    private function tallyScore()
    {
        foreach ($this->frames as $frameNumber => $frame) {
            // 10 frames per game
            if ($frameNumber > 10) {
                continue;
            }

            // get the raw frame score
            $frameTotal = $this->calculateRawScore($frame);

            // add the bonus frame score, if any
            if ($frameTotal == 10 && count($frame) == 1) {
                // strike, 2 frame bonus
                $frameTotal += $this->getTwoFrameBonus($frameNumber);
            } else if ($frameTotal == 10 && count($frame) == 2) {
                // spare, 1 frame bonus
                $frameTotal += $this->getOneFrameBonus($frameNumber);
            } else {
                // open frame, no bonus
            }

            $this->totalScore += $frameTotal;
        }

        return $this;
    }

    /**
     * Get the raw score of a frame without bonuses
     * @param  array   $frame
     * @return integer
     */
    private function calculateRawScore($frame)
    {
        $score = 0;

        foreach ($frame as $try) {
            $score += $try;
        }

        return $score;
    }

    /**
     * Get the bonus score of the next frame (for spares)
     * @param  integer $frameNumber 1-10
     * @return integer
     */
    private function getOneFrameBonus($frameNumber)
    {
        $bonus = 0;

        if (isset($this->frames[$frameNumber + 1])) {
            $bonus = $this->frames[$frameNumber + 1][0];
        }

        return $bonus;
    }

    /**
     * Get the bonus score of the next two frames (for strikes)
     * @param  integer $frameNumber 1-10
     * @return integer
     */
    private function getTwoFrameBonus($frameNumber)
    {
        $firstBonus = $this->getOneFrameBonus($frameNumber);

        if (isset($this->frames[$frameNumber + 1][1])) {
            $secondBonus = $this->frames[$frameNumber + 1][1];
        } else {
            $secondBonus = $this->getOneFrameBonus($frameNumber + 1);
        }

        return $firstBonus + $secondBonus;
    }
}
