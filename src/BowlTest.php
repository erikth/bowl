<?php
/**
 * Bowl class unit tests
 *
 * @package     Bowl
 * @author      Erik TH
 */
class BowlTest extends PHPUnit_Framework_Testcase
{
    public function testInstance()
    {
        $bowl = new Bowl('X');

        $this->assertInstanceOf('Bowl', $bowl);
    }

    public function testNoScore()
    {
        $this->setExpectedException('Exception',
            'no score given'
        );

        $bowl = new Bowl();
    }

    public function testPerfectGame()
    {
        $bowl = new Bowl('XXXXXXXXXXXX');

        $result = $bowl->run();

        $this->assertEquals(300, $result);
    }

    public function testGutterBalls()
    {
        $bowl = new Bowl('-----------');

        $result = $bowl->run();

        $this->assertEquals(0, $result);
    }

    public function testFouls()
    {
        $bowl = new Bowl('FFFFFFFFFFF');

        $result = $bowl->run();

        $this->assertEquals(0, $result);
    }

    public function testZeroOnePinsPerFrame()
    {
        $bowl = new Bowl('-1-1-1-1-1-1-1-1-1-1');

        $result = $bowl->run();

        $this->assertEquals(10, $result);
    }

    public function testOneZeroPinsPerFrame()
    {
        $bowl = new Bowl('1-1-1-1-1-1-1-1-1-1-');

        $result = $bowl->run();

        $this->assertEquals(10, $result);
    }

    public function testFiveZeroPinsPerFrame()
    {
        $bowl = new Bowl('5-5-5-5-5-5-5-5-5-5-');

        $result = $bowl->run();

        $this->assertEquals(50, $result);
    }

    public function testZeroFivePinsPerFrame()
    {
        $bowl = new Bowl('-5-5-5-5-5-5-5-5-5-5');

        $result = $bowl->run();

        $this->assertEquals(50, $result);
    }

    public function testZeroNinePinsPerFrame()
    {
        $bowl = new Bowl('-9-9-9-9-9-9-9-9-9-9');

        $result = $bowl->run();

        $this->assertEquals(90, $result);
    }

    public function testNineZeroPinsPerFrame()
    {
        $bowl = new Bowl('9-9-9-9-9-9-9-9-9-9-');

        $result = $bowl->run();

        $this->assertEquals(90, $result);
    }

    public function testOneOnePinsPerFrame()
    {
        $bowl = new Bowl('11111111111111111111');

        $result = $bowl->run();

        $this->assertEquals(20, $result);
    }

    public function testFourFourPinsPerFrame()
    {
        $bowl = new Bowl('44444444444444444444');

        $result = $bowl->run();

        $this->assertEquals(80, $result);
    }

    public function testStrikeNoBonus()
    {
        $bowl = new Bowl('X--X--X--X--X--');

        $result = $bowl->run();

        $this->assertEquals(50, $result);
    }

    public function testStrikeOnePinBonus()
    {
        $bowl = new Bowl('X1-X1-X1-X1-X1-X1-');

        $result = $bowl->run();

        $this->assertEquals(50 + 10, $result);
    }

    public function testStrikeTwoPinBonus()
    {
        $bowl = new Bowl('X2-X2-X2-X2-X2-X2-');

        $result = $bowl->run();

        $this->assertEquals(50 + 20, $result);
    }

    public function testStrikeThreePinBonus()
    {
        $bowl = new Bowl('X3-X3-X3-X3-X3-X3-');

        $result = $bowl->run();

        $this->assertEquals(50 + 30, $result);
    }

    public function testStrikeNinePinBonus()
    {
        $bowl = new Bowl('X9-X9-X9-X9-X9-X9-');

        $result = $bowl->run();

        $this->assertEquals(50 + 90, $result);
    }

    public function testOnePinSpares()
    {
        $bowl = new Bowl('1/1/1/1/1/1/1/1/1/1/1');

        $result = $bowl->run();

        $this->assertEquals(110, $result);
    }

    public function testTwoPinSpares()
    {
        $bowl = new Bowl('2/2/2/2/2/2/2/2/2/2/2');

        $result = $bowl->run();

        $this->assertEquals(120, $result);
    }

    public function testThreePinSpares()
    {
        $bowl = new Bowl('3/3/3/3/3/3/3/3/3/3/3');

        $result = $bowl->run();

        $this->assertEquals(130, $result);
    }

    public function testFivePinSpares()
    {
        $bowl = new Bowl('5/5/5/5/5/5/5/5/5/5/5');

        $result = $bowl->run();

        $this->assertEquals(150, $result);
    }

    public function testNinePinSpares()
    {
        $bowl = new Bowl('9/9/9/9/9/9/9/9/9/9/9');

        $result = $bowl->run();

        $this->assertEquals(190, $result);
    }

    public function testLastFrameTurkey()
    {
        $bowl = new Bowl('------------------XXX');

        $result = $bowl->run();

        $this->assertEquals(30, $result);
    }

    public function testLastFrameOneSpareStrike()
    {
        $bowl = new Bowl('------------------1/X');

        $result = $bowl->run();

        $this->assertEquals(20, $result);
    }

    public function testLastFrameFiveSpareStrike()
    {
        $bowl = new Bowl('------------------5/X');

        $result = $bowl->run();

        $this->assertEquals(20, $result);
    }

    public function testLastFrameNineSpareStrike()
    {
        $bowl = new Bowl('------------------9/X');

        $result = $bowl->run();

        $this->assertEquals(20, $result);
    }

    public function testConsecutiveIncrease()
    {
        $bowl = new Bowl('0123456/7/8/9/XXXXX');

        $result = $bowl->run();

        $this->assertEquals(179, $result);
    }

    public function testConsecutiveDecrease()
    {
        $bowl = new Bowl('X9/8/7/6/54321-----');

        $result = $bowl->run();

        $this->assertEquals(101, $result);
    }

    public function testOneStrike()
    {
        $bowl = new Bowl('X');

        $result = $bowl->run();

        $this->assertEquals(10, $result);
    }

    public function testOneSpare()
    {
        $bowl = new Bowl('5/');

        $result = $bowl->run();

        $this->assertEquals(10, $result);
    }

    public function testOneOpenFrame()
    {
        $bowl = new Bowl('5');

        $result = $bowl->run();

        $this->assertEquals(5, $result);
    }

}